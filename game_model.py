class GameModel:
  # Abstract game class

  def __init__(self):
    state = "" # state of the game
    currentPlayer = 1 # 1 - 1st player, 2 - 2nd player

  def isEnded(self):
    return "true or false"

  def winner(self):    
    return 0 # 1, 2 if isEnd()

  def listMoves(self):
    return "this should return a list of possible moves"

  def progress(self, move):  # process the game by a move
    return "this should process the game and return something"

  def score(self):
    return 0 # a number if isEnd(), + for 1st player win, - for 2nd. +-1 in case game don't have score