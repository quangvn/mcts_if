import checker
import mcts
import random_engine
import os

class Main():
  LOG_DIR = "D:/Work/Jaist/MCTS_IF/log/"
  def __init__(self, max_depth = 8):
    self.P1win = 0
    self.P2win = 0

  #### SIMULATION FUNCTIONS ####
  def simulateOneGameRandom(self):
    game = checker.Checker()
    rn = random.RandomEngine()

    while not game.isEnded():
      move = rn.getBestMove(game)
      # print "{0}: {1}".format(game.currentPlayer, move)
      game.progress(move)
      # game.printBoard()
    return game.winner()

  def simulateOneGameMCTS(self):
    game = checker.Checker()
    mc = mcts.MCTS()

    while not game.isEnded():
      move = mc.getBestMove(game)
      game.progress(move)
    return game.winner()

  def simulate(self, bangoukai):
    self.P1win = 0
    self.P2win = 0

    for i in range(bangoukai):
      result = self.simulateOneGameRandom()
      if result == 1:
        self.P1win += 1
      else:
        self.P2win += 1

    print "{0} - {1}".format(self.P1win, self.P2win)

  def newGameVsHuman(self):
    game = checker.Checker()
    mc = mcts.MCTS(8)

    while not game.isEnded():
      move = mc.getBestMove(game)
      print "computer chooses {0}".format(move)
      game.progress(move)
      game.printBoard()
      print "="*23
      if game.isEnded():
        break

      print "possible moves: {0}".format(game.listMoves())
      move = raw_input("please enter your move: ")
      game.progress(move)
      game.printBoard()
      print "="*23
    if game.winner() == 1:
      print "you lose"
    else:
      print "you win"

  def newGameBetweenEngine(self, e1, e1_name, e2, e2_name):
    game = checker.Checker()
    p1ep = []
    p2ep = []
    p1ewp = []
    p2ewp = []

    file_name = self.LOG_DIR + "{0}/match.log".format(id(game))
    d = os.path.dirname(file_name)
    if not os.path.exists(d):
      os.makedirs(d)
    output = file(file_name, 'a')
    
    while not game.isEnded():
      if game.step > 200:
        break
      # move = self.getBestMove(game)
      move = e1.getBestMove(game)
      p1ep.append(e1.eprob)
      p1ewp.append(e1.ewprob)

      print >>output, "Step {0}: {1} chooses {2}".format(game.step, e1_name, move)
      game.progress(move)
      game.printBoard(output)
      print >>output, "="*23
      if game.isEnded():
        break

      move = e2.getBestMove(game)
      p2ep.append(e2.eprob)
      p2ewp.append(e2.ewprob)

      print >>output, "Step {0}: {1} chooses {2}".format(game.step, e2_name, move)
      game.progress(move)
      game.printBoard(output)
      print >>output, "="*23
      output.flush()
    if game.winner() == 1:
      print >>output, "{0} win".format(e1_name)
    else:
      print >>output, "{0} win".format(e2_name)
    output.flush()
    output.close()

    # log player 1 expectation value
    file_name = self.LOG_DIR + "{0}/p1-e.log".format(id(game))
    output = file(file_name, "w")
    for i in range(1, len(p1ep)):
      print >>output, p1ep[i-1]*(1-abs(p1ewp[i]-p1ewp[i-1]))
    output.flush()
    output.close()

    # log player 2 expectation value
    file_name = self.LOG_DIR + "{0}/p2-e.log".format(id(game))
    output = file(file_name, "w")
    for i in range(1, len(p2ep)):
      print >>output, p2ep[i-1]*(1-abs(p2ewp[i]-p2ewp[i-1]))
    output.flush()
    output.close()

    return game.winner()

  def simulateMCTSvsRandom(self, bangoukai):
    self.P1win = 0
    self.P2win = 0
    mc = mcts.MCTS(8)
    rn = random_engine.RandomEngine()


    for i in range(bangoukai):
      result = self.newGameBetweenEngine(mc, "MCTS", rn, "Random")
      if result == 1:
        self.P1win += 1
      else:
        self.P2win += 1

    print "{0} - {1}".format(self.P1win, self.P2win)

  def simulateMCTSvsMCTS(self, bangoukai):
    self.P1win = 0
    self.P2win = 0
    mc1 = mcts.MCTS(8, True)
    mc2 = mcts.MCTS(8, True)

    for i in range(bangoukai):
      result = self.newGameBetweenEngine(mc1, "MCTS_1", mc2, "MCTS_2")
      if result == 1:
        self.P1win += 1
      else:
        self.P2win += 1

    print "{0} - {1}".format(self.P1win, self.P2win)

  # import
  def read_input(self, file_name):
    f = file(file_name,'r')
    moves = []
    for line in f:
      if line[0] == '[':
        continue
      moves += line.split()

    return moves[0:-1]

  # analyze
  def analyze_match(self, file_name):
    moves = self.read_input(file_name)
    mc1 = mcts.MCTS(8, True)
    mc2 = mcts.MCTS(8, True)
    game = checker.Checker()

    file_name = self.LOG_DIR + "{0}/match.log".format(id(game))
    d = os.path.dirname(file_name)
    if not os.path.exists(d):
      os.makedirs(d)

    for i in reversed(range(0,len(moves))):
      if moves[i].find('.') > 0:
        del moves[i]

    for i in range(0, len(moves)):
      if i%2 == 0:
        mc1.getBestMove(game)
      else:
        mc2.getBestMove(game)
      game.progress(self.convert_pdn(moves[i], game))

  # others
  def find_capture_sequence(self, st, fn, game):
    moves = game.listMoves()
    for move in moves:
      if move.startswith("C" + st) and move.endswith(fn):
        return move

  def convert_position(self, pos):
    p = int(pos)
    x = (p-1)/4
    y = ((p-1)%4)*2+1-x%2
    return "{0}{1}".format(x,y)

  def convert_pdn(self, pdn, game):
    if pdn.find('-') > 0: # normal move
      move = ""
      pos = pdn.split('-')
      for i in range(0,len(pos)):
        move += self.convert_position(pos[i])
      return move

    else: # capture move
      move = "C"
      pos = pdn.split('x')
      for i in range(0,len(pos)):
        pos[i] = self.convert_position(pos[i])
      if len(pos) > 2: # full capture sequence
        for p in pos:
          move += p
        return move
      else: # only start and end position
        return self.find_capture_sequence(pos[0], pos[1], game)
