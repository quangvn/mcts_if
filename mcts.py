import checker
import random
import math
import copy
import sys
import time
import StringIO
import os

class Node:
  EXPLORE_CONST = 2.0

  def __init__(self, position, parent, prev_move):
    self.position = position # should check shallow copy vs deep copy

    self.parent = parent
    if self.parent:
      self.depth = self.parent.depth + 1
    else:
      self.depth = 1

    self.prev_move = prev_move

    self.visit_times = 0.0
    self.win_times = 0.0

    self.children = {}
    self.list_moves = position.listMoves()

  def chooseBestMove(self):
    return max(self.children, key=lambda i: self.children[i].win_times/self.children[i].visit_times)

  def compute_ucb(self):
    if self.parent is not None:
      # print "win: {0}, visit: {1}, parent_visit: {2}".format(self.win_times, self.visit_times, self.parent.visit_times)
      return self.win_times/self.visit_times + self.EXPLORE_CONST*((math.log(self.parent.visit_times, math.e)/self.visit_times) ** 0.5)
    # only root don't have parent, and root don't need to calculate ucb
    else:
      pass

  def selectNext(self):
    # haven't try all possible moves
    if len(self.children) < len(self.list_moves):
      # try the next new move
      move = self.list_moves[len(self.children)]
      next_position = copy.deepcopy(self.position)
      next_position.progress(move)
      # add to tree - expansion
      self.children[move] = Node(next_position, self, move)
      return self.children[move]
    # try the move that maximizes ucb_value
    return self.children[max(self.children, key=lambda i: self.children[i].compute_ucb())]

  def calculateEntropy(self):
    queue = [[self,1.0]] # node, probs
    probs = []
    while len(queue):
      current_node, cprobs = queue.pop()
      if len(current_node.children) == 0:
        probs.append(cprobs)
      else:
        for child in current_node.children:
          queue.append([current_node.children[child], cprobs * current_node.children[child].visit_times / current_node.visit_times])
    res = 0.0
    for p in probs:
      res += -p*math.log(p,2)
    return res

  def calculateSquaredEntropy(self):
    queue = [[self,1.0]] # node, probs
    probs = []
    while len(queue):
      current_node, cprobs = queue.pop()
      if len(current_node.children) == 0:
        probs.append(cprobs)
      else:
        for child in current_node.children:
          queue.append([current_node.children[child], cprobs * current_node.children[child].visit_times / current_node.visit_times])
    res = 0.0
    for p in probs:
      res += (p*math.log(p,2))**2
    return res

  def calculateEntropy2(self):
    queue = [[self,1.0]] # node, probs
    probs = []
    while len(queue):
      current_node, cprobs = queue.pop()
      if len(current_node.children) == 0:
        probs.append(cprobs)
      else:
        probs.append(1.0/self.visit_times)
        for child in current_node.children:
          queue.append([current_node.children[child], cprobs * current_node.children[child].visit_times / current_node.visit_times])
    res = 0.0
    for p in probs:
      res += -p*math.log(p,2)
    return res


  def children_ids(self):
    output = StringIO.StringIO()
    for child in self.children:
      output.write("{0} ".format(id(self.children[child])))
    content = output.getvalue()
    output.close()
    return content


class MCTS:
  TRIAL_CONST = 4000
  LOG_DIR = "D:/Work/Jaist/MCTS_IF/log/"

  def __init__(self, max_depth = 8, logging = False):
    self.max_tree_depth = max_depth
    self.logging = logging

  def simulateCustomPosition(self, game):
    while not game.isEnded():
      list_moves = game.listMoves()
      move = random.choice(list_moves)
      game.progress(move)
    return game.winner()

  # may need to change
  def evaluate(self, node, score):
    if node.position.currentPlayer == score:
      return 1
    return 0

  def getBestMove(self, game):
    # create a completely new tree
    # Note: reusing the tree from last moves might be better
    # the new root should be a grandchild of the last root node (in case of a real game)
    # many playouts may have go through it
    root = Node(game, None, None)
    trial = 0

    while trial < self.TRIAL_CONST:
      current_node = root
      # if current_node has been added, choose moves according to ucb value
      while True:
        current_node = current_node.selectNext()
        if current_node.visit_times == 0 or current_node.position.isEnded() or current_node.depth == self.max_tree_depth:
          break

      # current_node is now an terminal node or a new node
      score = self.simulateCustomPosition(copy.deepcopy(current_node.position))

      # evaluation
      v = self.evaluate(root, score)

      # backpropagation
      while current_node is not None:
        # update visit_times, win_times
        current_node.visit_times += 1
        current_node.win_times += v
        current_node = current_node.parent
      trial += 1

    # for expectation value
    best_move = root.chooseBestMove()
    c1 = root.children[best_move]
    if len(c1.children):
      c2 = c1.children[c1.chooseBestMove()]
      self.eprob = c2.visit_times / c1.visit_times
      self.ewprob = c2.win_times / c2.visit_times

    if self.logging:
      self.logTree(root, self.LOG_DIR + "{0}/{1}.log".format(id(game), game.step))
      self.logIValue(root, self.LOG_DIR + "{0}/players.log".format(id(game)))
    # choose best move
    return best_move

#### LOG FUNCTIONS ####
  def logIValue(self, root, file_name):
    f = file(file_name, 'a')
    f.write("{0}\t{1}\t{2}\n".format(root.calculateEntropy(), root.calculateEntropy2(), root.calculateSquaredEntropy()))
    f.close()

    if root.position.step % 2 == 0:
      f = file(file_name + "_1", 'a')
      f.write("{0}\t{1}\t{2}\n".format(root.calculateEntropy(), root.calculateEntropy2(), root.calculateSquaredEntropy()))
      f.close()
    else:
      f = file(file_name + "_2", 'a')
      f.write("{0}\t{1}\t{2}\n".format(root.calculateEntropy(), root.calculateEntropy2(), root.calculateSquaredEntropy()))
      f.close()

  def logTree(self, root, file_name):
    f = file(file_name, 'w')
    # log first level
    f.write("{0} {1}\n".format(root.visit_times, root.win_times))
    tmp = ""
    for child in root.children:
      tmp = tmp + str(root.children[child].visit_times) + " "
    f.write(tmp + "\n")

    tmp = ""
    for child in root.children:
      tmp = tmp + str(root.children[child].win_times) + " "
    f.write(tmp + "\n")

    tmp = ""
    for child in root.children:
      tmp = tmp + str(root.children[child].win_times/root.children[child].visit_times) + " "
    f.write(tmp + "\n")

    tmp = ""
    for child in root.children:
      tmp = tmp + str(root.children[child].compute_ucb()) + " "
    f.write(tmp + "\n")

    # full tree log
    queue = [root]
    while len(queue):
      current_node = queue.pop()
      f.write("%(i)d %(v)d %(w)d %(c)s\n" % {'i':id(current_node), 'v':current_node.visit_times, 'w':current_node.win_times, 'c':current_node.children_ids()})
      for child in current_node.children:
        queue.append(current_node.children[child])
    f.close()

