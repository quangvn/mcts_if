import game_model
import random

class RandomEngine():
  def __init__(self):
    pass    

  def getBestMove(self, game):
    return random.choice(game.listMoves())