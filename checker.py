import game_model
import sys

class Checker():
  # men's moves
  # 1P         2P  
  # 0 0 0      1 0 2
  # 0 M 0      0 M 0 
  # 1 0 2      0 0 0
  # vertical moves of men
  men_moves_x = [[0,0], # added for convenient index
                 [1,1],   # player 1
                 [-1,-1]] # player 2
  # horizontal moves of men
  men_moves_y = [[0,0],
                 [-1,1], # player 1
                 [-1,1]] # player 2


  # king's moves
  #  3 0 4
  #  0 K 0
  #  1 0 2
  king_moves_x = [1,1,-1,-1]

  king_moves_y = [-1,1,-1,1]

  def __init__(self):
    # 0: empty square
    # 1: 1P men
    # 2: 2P men
    # -1: 1P kings
    # -2: 2P kings
    self.board = [[0,1,0,1,0,1,0,1],
                  [1,0,1,0,1,0,1,0],
                  [0,1,0,1,0,1,0,1],
                  [0,0,0,0,0,0,0,0],
                  [0,0,0,0,0,0,0,0],
                  [2,0,2,0,2,0,2,0],
                  [0,2,0,2,0,2,0,2],
                  [2,0,2,0,2,0,2,0]]
    self.step = 0
    self.p1_men = 12
    self.p1_kings = 0
    self.p2_men = 12
    self.p2_kings = 0
    self.currentPlayer = 1

  def p1_total(self):
    return self.p1_men + self.p1_kings

  def p2_total(self):
    return self.p2_men + self.p2_kings

  def isEnded(self):
    if (self.p1_total == 0) or (self.p2_total == 0):
      return True
    if (len(self.listMoves()) == 0):
      return True
    return False

  def winner(self):    
    if self.isEnded():
      # current player is the one who lost
      return 3-self.currentPlayer 
    return 0

  def inBoard(self, x, y):
    if x >= 0 and y >= 0 and x < 8 and y < 8:
      return True
    return False

  # list of valid moves for current player from x-y, no capture moves
  def validMoves(self, x, y):
    moves = []

    if self.board[x][y] > 0: # man
      # check man's moves
      for i in range(len(self.men_moves_x[self.currentPlayer])):
        nx = x + self.men_moves_x[self.currentPlayer][i]
        ny = y + self.men_moves_y[self.currentPlayer][i]
        if self.inBoard(nx, ny) and self.board[nx][ny] == 0:
          moves.append("{0}{1}{2}{3}".format(x,y,nx,ny))
    else:
      # check king's moves
      for i in range(len(self.king_moves_x)):
        nx = x + self.king_moves_x[i]
        ny = y + self.king_moves_y[i]
        if self.inBoard(nx, ny) and self.board[nx][ny] == 0:
          moves.append("{0}{1}{2}{3}".format(x,y,nx,ny))

    return moves

  def generateMenCaptureSequences(self, x, y, moves, seq, clen):
    capture_end = True
    for i in range(len(self.men_moves_x[self.currentPlayer])):
      n1x = x + self.men_moves_x[self.currentPlayer][i]
      n1y = y + self.men_moves_y[self.currentPlayer][i]
      n2x = n1x + self.men_moves_x[self.currentPlayer][i]
      n2y = n1y + self.men_moves_y[self.currentPlayer][i]
      if self.validCaptureMove(x, y, n1x, n1y, n2x, n2y):
        capture_end = False
        self.generateMenCaptureSequences(n2x, n2y, moves, seq + "{0}{1}".format(n2x,n2y), clen + 1)

    if capture_end and clen > 0:
      moves.append(seq)

  def generateKingCaptureSequences(self, x, y, moves, seq, clen, prev_dir):
    capture_end = True
    for i in range(len(self.king_moves_x)):
      if i == prev_dir:
        continue
      n1x = x + self.king_moves_x[i]
      n1y = y + self.king_moves_y[i]
      n2x = n1x + self.king_moves_x[i]
      n2y = n1y + self.king_moves_y[i]
      temp = 0
      if self.validCaptureMove(x, y, n1x, n1y, n2x, n2y):
        capture_end = False
        temp = self.board[n1x][n1y]
        self.board[n1x][n1y] = 0
        self.generateKingCaptureSequences(n2x, n2y, moves, seq + "{0}{1}".format(n2x,n2y), clen + 1, 3-i)
        self.board[n1x][n1y] = temp

    if capture_end and clen > 0:
      moves.append(seq)

  def validCaptureMove(self, x, y, n1x, n1y, n2x, n2y):
    if self.inBoard(n2x, n2y) and self.board[n1x][n1y] != 0 and abs(self.board[n1x][n1y]) != self.currentPlayer and self.board[n2x][n2y] == 0:
      return True
    return False

  # list of valid capture moves for current player from x-y
  def validCaptureMoves(self, x, y):
    moves = []

    if self.board[x][y] > 0: # man
      self.generateMenCaptureSequences(x, y, moves, "C{0}{1}".format(x, y), 0)
    else:
      self.generateKingCaptureSequences(x, y, moves, "C{0}{1}".format(x, y), 0, -1)

    return moves

  def listMoves(self):
    moves = []
    capture_moves = []

    for i in range(len(self.board)):
      for j in range(len(self.board[i])):
        # check every squares
        if abs(self.board[i][j]) == self.currentPlayer:
          capture_moves += self.validCaptureMoves(i,j)
          moves += self.validMoves(i,j)

    if len(capture_moves) > 0:
      return capture_moves
    return moves

  def progress(self, move):  # progress the game by a move
    # for now, assume that the input move is always a legal move
    if move[0] == "C":  # capture sequence
      cmen = 0
      cking = 0
      x = int(move[1])
      y = int(move[2])
      cur = 3
      while cur < len(move):
        nx = int(move[cur])
        ny = int(move[cur+1])
        cur += 2

        if self.board[(x+nx)/2][(y+ny)/2] > 0:
          cmen += 1
        else:
          cking += 1

        self.board[(x+nx)/2][(y+ny)/2] = 0
        self.board[nx][ny] = self.board[x][y] # TODO remove this
        self.board[x][y] = 0

        x = nx
        y = ny

      if self.currentPlayer == 1:
        self.p2_kings -= cking
        self.p2_men -= cmen
      else:
        self.p1_kings -= cking
        self.p1_men -= cmen

    else: # normal move
      x = int(move[0])
      y = int(move[1])
      self.board[int(move[2])][int(move[3])] = self.board[x][y]
      self.board[x][y] = 0

    # promote if possible
    x = int(move[-2])
    y = int(move[-1])
    if self.currentPlayer == 1 and x == len(self.board)-1 and self.board[x][y] == 1:
      self.board[x][y] = -1
      self.p1_men -= 1
      self.p1_kings += 1
    elif self.currentPlayer == 2 and x == 0 and self.board[x][y] == 2:
      self.board[x][y] = -2
      self.p2_men -= 1
      self.p2_kings += 1

    self.currentPlayer = 3 - self.currentPlayer # change turn
    self.step += 1


  def score(self):
    if self.winner() == 1:
      return 1
    elif self.winner() == 2:
      return -1
    return 0

  def printBoard(self, output = sys.stdout):
    for i in self.board:
      s = ""
      for j in i:
        if j == 1:
          s += "(x)"
        elif j == -1:
          s += "[X]"
        elif j == 2:
          s += "(o)"
        elif j == -2:
          s += "[O]"
        else:
          s += " _ "
      print >>output, s
